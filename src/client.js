import Axios from 'axios'

class WebhookClient {
  constructor(...args) {
    let config = {},
      urlScheme = 'http://',
      baseURL

    if (typeof arguments[0] == 'object') {
      config = arguments[0]
      if (config.secure) {
        urlScheme = 'https://'
      }
      baseURL = urlScheme + config.host || config.hostname + ':' + config.port
    } else if (typeof arguments[0] == 'string') {
      baseURL = urlScheme + arguments[0]
    }

    config.baseURL = baseURL

    this.config = config
  }

  async run(options) {
    const {
      data
    } = await Axios.post(this.config.baseURL, options)

    return data
  }
}

export default WebhookClient
